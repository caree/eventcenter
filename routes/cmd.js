

/*
* 最外层: {status:'success', cmds:[主体内容]}
* 可以根据status判定是否需要主体内容
*
* Command 的 para 参数,各个不同的需求可以自行定义
*/
var timeFormater = require('./timeFormat').getCurrentTime;
var _ = require('underscore');
var wssModule = require('./wss');

var commandList = [];

setInterval(function(){
	var timeStamp = (new Date()).getTime();
	var initialLength = _.size(commandList);
	commandList = _.filter(commandList, function(_command){
		return (timeStamp - _command.timeStamp) < _command.duration;
	});
	var currentLength = _.size(commandList);
	if(initialLength > currentLength){
		console.log('commandList expire => '.info);
		console.dir(commandList);
	}
}, 1000);


function Command(_name, _para){
	this.name = _name;
	this.para = _para;
	// this.msgType = '';
	this.timeStamp = (new Date()).getTime();
	this.duration = ExpireTimeLength;//ms
}
Command.prototype.tryValidate = function() {
	if(this.name == null || this.name.length <= 0){
		return false;
	}else return true;
	// body...
};

exports.newcmd = function(_json_cmd){
	// console.dir(_json_cmd);
	var newcmd = new Command(_json_cmd.name, _json_cmd.para);
	if(!newcmd.tryValidate()) return "paraError";
	
	if(_json_cmd.duration != null){
		newcmd.duration = _json_cmd.duration;
	}
    console.log('newcmd =>'.data);
    console.dir(newcmd);
    g_ep.emit('msg', timeFormater() + ' ' + JSON.stringify(newcmd));
	var cmds2send = getCmdListByName(newcmd.name);
	cmds2send.push(newcmd);
	
    var bConsumed = wssModule.wss_broadcast(newcmd.name, cmds2send);

    if(bConsumed == false){
    	var log = 'do not consumed by wss , add to list';
    	console.log(log.data);
	    commandList.push(newcmd);
	    g_ep.emit('msg', timeFormater() + ' ' + log);
    }else{
    	var log = 'consumed by wss , clear all ' + newcmd.name + ' command';
    	console.log(log.data);
    	clearCmdByName(newcmd.name);
	    g_ep.emit('msg', timeFormater() + ' ' + log);
    }
    return 'ok';	
};

exports.getcmds  = function(_json_cmd){
	var cmds2send = getCmdListByName(_json_cmd.name);
	clearCmdByName(_json_cmd.name);
	return cmds2send;
    // return {status:'success', cmds:JSON.stringify(cmds2send)};
};


function getCmdListByName(_name){
	console.log(('getCmdListByName => ' + _name).data);
	// console.dir(commandList);
	var arrayWithSpecifiedName = _.filter(commandList, function(_command){
		return _command.name == _name;
	});
	return arrayWithSpecifiedName;
}
function clearCmdByName(_name){
	commandList = _.filter(commandList, function(_command){
		return _command.name != _name;
	});
}


