

/**
 * 特性：
 * 支持ws和http，两者融合在一起
 * 进入的消息放到统一的消息堆栈中
 * 核心在于命令的消费:http方式提交的同时,提出消费类别;ws方式连接时提供消费类别
 * {msgType: 'pull|push|reg', content: '[cmds]'}
 */

var _               = require('underscore');
var cmd             = require('./cmd');
var WebSocketServer = require('ws').Server;
var wss             = null;
var clients         = [];
var timeFormater    = require('./timeFormat').getCurrentTime;

g_ep.tail('msg', broadcastData);

exports.startWssServer = function(httpServer){
  
  wss = new WebSocketServer({server : httpServer});
  wss.on('connection', function(ws){

  ws.cmdRegList = [];
  ws.clientType = 'normal';  
  clients.push(ws);
  g_ep.emit('msg', timeFormater() + ' new client connected!');

    ws.on('open', function(){
    });

    ws.on('message', function(msg){
      console.log(('message => '+ msg).data);
      var json_cmd  = JSON.parse(msg);
      if(json_cmd.msgType == null) return;

      if(json_cmd.msgType == 'pull'){
        var jsonReturn = cmd.getcmds(json_cmd.name);
        ws.send(JSON.stringify(jsonReturn));
      }

      if(json_cmd.msgType == 'push'){
        var jsonReturn = cmd.newcmd(json_cmd);
        ws.send(JSON.stringify(jsonReturn));
      }

      if(json_cmd.msgType == 'reg'){
        ws.cmdRegList.push(json_cmd.name);
        var jsonReturn    = cmd.getcmds(json_cmd);
        ws.send(JSON.stringify(jsonReturn));
      }

      if(json_cmd.msgType == 'showdata'){
        ws.clientType = 'showdata';
      }

    });

    ws.on('close', function(){
      var index = clients.indexOf(ws);
      if(index >= 0){
        clients.splice(index, 1);
      }
      console.log('close =>');
      g_ep.emit('msg', timeFormater() + ' client close!');
    });
    
  });
};
function broadcastData(_msg){
  _.chain(clients).filter(function(_client){
    return _client.clientType == 'showdata';
  }).each(function(_client){
    _client.send(_msg);
  })
}
//查找该ws是否订阅了该名称的命令,如果订阅则将命令发送出去
exports.wss_broadcast = function(cmdName, json_data) {
    console.log(('wss_broadcast => ' + cmdName).data);
    console.dir(json_data);
    var bConsumed = false;
    _.each(clients, function(_client){
        console.log('reg list => '.data);
        console.dir(_client.cmdRegList);
        if(_.contains(_client.cmdRegList, cmdName)){
          bConsumed = true;
          _client.send(JSON.stringify(json_data));
        }
    });
    return bConsumed;
};


