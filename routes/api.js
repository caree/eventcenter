/*
说明：
 * 本消息服务的目的为服务需求层和实现层提供以消息的形式传递信息方式
 * 支持ws和http两种方式提交消息，进入的消息放到统一的消息堆栈中
 * 消息的格式为：{msgType: 'pull|push|reg', para: '', name: ''}
 * 对于ws方式，提交的消息类型分为三种 pull push 和 reg，也就是 msgType 参数的内容
   其中，push表示要向系统提交消息，pull表示要获取消息，reg表示要订阅消息
   之后的 name 表示提交、获取或者订阅的消息名称，para 则可以存放该消息的一些自定义参数

 * 

 HTTP API：
 	1. 添加消息
 		URL： http://localhost:6013/newcmd
 		header: Content-Type: text/plain
 		body: {"name":"newProductionOrder","msgType":"push","para":""}
		成功则返回发送的消息原文

	2. 获取消息
 		URL： http://localhost:6013/getcmds
 		header: Content-Type: text/plain
 		body: {"name":"newProductionOrder","msgType":"","para":""}
		成功返回的消息格式		
 		说明：参数中 name 参数不可缺少

 Websocket API
 	1. 建立连接后，发送reg类型消息，如 
 	   {"name":"newProductionOrder","msgType":"push","para":""}
 	2. 当订阅的消息类型中有新消息时，会接收到该新消息


Content-Type: application/json; charset=UTF-8
{"carID":"led"}
*/