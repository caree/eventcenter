
var cmd = require('./cmd');
var timeFormater = require('./timeFormat').getCurrentTime;


exports.newcmd = function(req, res){
    var body     = req.body;
    var name     = body.name;
    var para     = body.para;
    var duration = body.duration;
    // var rawBody = req.rawBody;
    // console.log(rawBody);
    // var json_cmd = JSON.parse(rawBody);
    var r = cmd.newcmd({name: name, para: para, duration: duration});
    res.send(r);
    return;	
};

exports.getcmds  = function(req, res){
    var name       = req.body.name;
	// var rawBody = req.rawBody;
	// var json_cmd = JSON.parse(rawBody);

	var jsonReturn = cmd.getcmds({name: name});
    var strJson    = JSON.stringify(jsonReturn);
    console.log(strJson.data); 
    g_ep.emit('msg', timeFormater() + ' getcmds => ' + strJson);
    res.send(strJson);
    
    return;
};

exports.index = function(req, res){
    res.render('wsTest');
};
exports.api = function(req, res){
    res.render('indexAPI');
}
exports.wsTest = function(req, res){
	res.render('wsTest');
};
// function getCmdListByName(_name){
// 	var arrayWithSpecifiedName = new Array();
// 	global.commandList.forEach(function(_cmd){
// 		if(_cmd.name == _name){
// 			arrayWithSpecifiedName.push(_cmd);
// 		}
// 	});
// 	return arrayWithSpecifiedName;
// }


